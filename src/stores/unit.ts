import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import unitService from '@/services/unit'
import type { Unit } from '@/types/Unit'

export const useUnitStore = defineStore('unit', () => {
  const loadingStore = useLoadingStore()
  const units = ref<Unit[]>([])
  const initialUnit: Unit = {
    name: ''
  }
  const editedUnit = ref<Unit>(JSON.parse(JSON.stringify(initialUnit)))

  async function getUnit(id: number) {
    loadingStore.doLoad()
    const res = await unitService.getUnit(id)
    editedUnit.value = res.data
    loadingStore.finish()
  }
  async function getUnits() {
    try {
      loadingStore.doLoad()
      const res = await unitService.getUnits()
      units.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveUnit() {
    loadingStore.doLoad()
    const unit = editedUnit.value
    if (!unit.id) {
      // Add new
      console.log('Post ' + JSON.stringify(unit))
      const res = await unitService.addUnit(unit)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(unit))
      const res = await unitService.updateUnit(unit)
    }

    await getUnits()
    loadingStore.finish()
  }
  async function deleteUnit() {
    loadingStore.doLoad()
    const unit = editedUnit.value
    const res = await unitService.delUnit(unit)

    await getUnits()
    loadingStore.finish()
  }

  function clearForm() {
    editedUnit.value = JSON.parse(JSON.stringify(initialUnit))
  }
  return { units, getUnits, saveUnit, deleteUnit, editedUnit, getUnit, clearForm }
})
