import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import materialService from '@/services/material'
import type { Material } from '@/types/Material'
import { useMessageStore } from './message'

export const useMaterialStore = defineStore('material', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const materials = ref<Material[]>([])
  const initialMaterial: Material & { files: File[] } = {
    name: '',
    qty: 0,
    price: 0,
    unit: { id: 2, name: 'material' },
    image: 'noimage.jpg',
    files: []
  }
  const editedMaterial = ref<Material & { files: File[] }>(
    JSON.parse(JSON.stringify(initialMaterial))
  )

  async function getMaterial(id: number) {
    try {
      loadingStore.doLoad()
      const res = await materialService.getMaterial(id)
      editedMaterial.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getMaterials() {
    try {
      loadingStore.doLoad()
      const res = await materialService.getMaterials()
      materials.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveMaterial() {
    try {
      loadingStore.doLoad()
      const material = editedMaterial.value
      if (!material.id) {
        // Add new
        console.log('Post ' + JSON.stringify(material))
        const res = await materialService.addMaterial(material)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(material))
        const res = await materialService.updateMaterial(material)
      }

      await getMaterials()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteMaterial() {
    loadingStore.doLoad()
    const material = editedMaterial.value
    const res = await materialService.delMaterial(material)

    await getMaterials()
    loadingStore.finish()
  }

  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
  }
  return {
    materials,
    getMaterials,
    saveMaterial,
    deleteMaterial,
    editedMaterial,
    getMaterial,
    clearForm
  }
})
