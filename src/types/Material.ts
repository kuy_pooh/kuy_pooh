import type { Unit } from './Unit'

type Material = {
  id?: number
  name: string
  qty: number
  price: number
  unit: Unit
  image: string
}
function getImageUrl(material: Material) {
  return `/img/coffees/material${material.id}.png`
}
export { type Material, getImageUrl }
