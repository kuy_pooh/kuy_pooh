import type { Material } from '@/types/Material'
import http from './http'

function addMaterial(material: Material & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', material.name)
  formData.append('price', material.qty.toString())
  formData.append('price', material.price.toString())
  formData.append('unit', JSON.stringify(material.unit))
  if (material.files && material.files.length > 0) formData.append('file', material.files[0])
  return http.post('/materials', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateMaterial(material: Material & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', material.name)
  formData.append('qty', material.qty.toString())
  formData.append('price', material.price.toString())
  formData.append('unit', JSON.stringify(material.unit))
  if (material.files && material.files.length > 0) formData.append('file', material.files[0])
  return http.post(`/materials/${material.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delMaterial(material: Material) {
  return http.delete(`/materials/${material.id}`)
}

function getMaterial(id: number) {
  return http.get(`/materials/${id}`)
}

function getMaterials() {
  console.log('Bearer ' + localStorage.getItem('access_token'))

  return http.get('/materials')
}

export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials }
